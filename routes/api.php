<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\JuegosController;
use App\Http\Controllers\UsuariosController;
use App\Http\Controllers\VentasController;
use App\Http\Controllers\DetallesVentasController;
use App\Http\Controllers\OpinionesController;
use App\Http\Controllers\AuthController;



Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::get('juegos',[JuegosController::class,'index']);
Route::resource('juegos', JuegosController::class);
Route::resource('usuarios', UsuariosController::class);
Route::resource('ventas', VentasController::class);
Route::resource('detalles_ventas', DetallesVentasController::class);
Route::resource('opiniones', OpinionesController::class);


