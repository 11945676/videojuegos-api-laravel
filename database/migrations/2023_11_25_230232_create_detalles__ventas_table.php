<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('detalles__ventas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_venta')->constrained('ventas')->onUpdate('cascade')->onDelete('restrict');
            $table->foreignId('id_juego')->constrained('juegos')->onUpdate('cascade')->onDelete('restrict');            
            $table->integer('Cantidad');
            $table->integer('Precio_Unitario');
            $table->integer('Subtotal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('detalles__ventas');
    }
};
