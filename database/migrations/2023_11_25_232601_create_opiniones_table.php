<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('opiniones', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_juego')->constrained('juegos')->onUpdate('cascade')->onDelete('restrict');
            $table->foreignId('id_usuario')->constrained('usuarios')->onUpdate('cascade')->onDelete('restrict');
            $table->integer('calificación');
            $table->string('Comentario',600);
            $table->date('Fecha_Opinion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('opiniones');
    }
};
