<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Opiniones extends Model
{
    use HasFactory;

    protected $fillable = ['id_juego', 'id_usuario', 'calificación', 'Comentario', 'Fecha_Opinion'];
}
