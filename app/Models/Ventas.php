<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ventas extends Model
{
    use HasFactory;
    protected $fillable = ['id_usuario', 'Fecha_Venta', 'Total_Venta', 'Metodo_Pago'];
}
