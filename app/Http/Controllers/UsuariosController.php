<?php

namespace App\Http\Controllers;

use App\Models\Usuarios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $usuarios = Usuarios::all();
        return response()->json($usuarios);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
         $rules = [
        'usuario' => 'required|string',
        'email' => 'required|string',
        'password' => 'required|string'
    ];

    $validator = Validator::make($request->input(),$rules);
    if($validator->fails()){
        return response()->json([
            'status' => false,
            'errors' => $request->errors()->all()
        ],400);
    }
        $usuarios = new Usuarios($request->input());
        $usuarios->save();

    return response()->json([
        'status' => true,
        'message' => 'juego created successfully'
    ],200);

    
    }

    /**
     * Display the specified resource.
     */
    public function show(Usuarios $usuarios)
    {
        return response()->json(['status' => true, 'data' => $usuarios]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Usuarios $usuarios)
    {
        $rules = [
            'usuario' => 'required|string',
            'email' => 'required|string',
            'password' => 'required|string'
        ];
    
        $validator = Validator::make($request->input(),$rules);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $request->errors()->all()
            ],400);
        }
            $usuarios->update($request->input());
            $usuarios->save();
        
        return response()->json([
            'status' => true,
            'message' => 'usuario created successfully'
        ],200);
    }

    public function destroy(Usuarios $usuarios)
    {
        $usuarios->delete();
        return response()->json([
            'status' => true,
            'message' => 'usuario deleted successfully'
        ],200);
    }

}
