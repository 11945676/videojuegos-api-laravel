<?php

namespace App\Http\Controllers;

use App\Models\Opiniones;
use Illuminate\Http\Request;

class OpinionesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Opiniones $opiniones)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Opiniones $opiniones)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Opiniones $opiniones)
    {
        //
    }
}
