<?php

namespace App\Http\Controllers;

use App\Models\Juegos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class JuegosController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $juegos = Juegos::all();
        return response()->json($juegos);
    }

    public function store(Request $request)
    {
        $rules = [
        'Nombre' => 'required|string',
        'Genero' => 'required|string',
        'Desarrolladora' => 'required|string',
        'F_Lanzamiento' => 'required|date',
        'Plataforma' => 'required|string'
    ];
    $validator = Validator::make($request->input(),$rules);
    if($validator->fails()){
        return response()->json([
            'status' => false,
            'errors' => $request->errors()->all()
        ],400);
    }
        $juegos = new Juegos($request->input());
        $juegos->save();

    return response()->json([
        'status' => true,
        'message' => 'juego created successfully'
    ],200);

    }


    public function show(Juegos $juegos)
    {
        return response()->json(['status' => true, 'data' => $juegos]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Juegos $juegos)
    {
        $rules = [
            'Nombre' => 'required|string',
            'Genero' => 'required|string',
            'Desarrolladora' => 'required|string',
            'F_Lanzamiento' => 'required|date',
            'Plataforma' => 'required|string'
        ];
        $validator = Validator::make($request->input(),$rules);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $request->errors()->all()
            ],400);  
        }
    
    $juegos->update($request->input());
    return response()->json([
        'status' => true,
        'message' => 'usuario updated successfully'
    ],200);

    }

    public function destroy(Juegos $juegos)
    {
        $juegos->delete();
        return response()->json([
            'status' => true,
            'message' => 'juego deleted successfully'
        ],200);
    }
}
